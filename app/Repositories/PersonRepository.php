<?php

namespace App\Repositories;

use App\Models\Person;

class PersonRepository
{
    public function create($data): Person
    {
        return Person::create($data);
    }

    public function update(Person $person, $data)
    {
        return $person->update($data);
    }
}
