<?php

namespace App\Repositories;

use App\Models\Level;

class LevelRepository
{
    public function create($data): Level
    {
        return Level::create($data);
    }

    public function update(Level $person, $data)
    {
        return $person->update($data);
    }
}
