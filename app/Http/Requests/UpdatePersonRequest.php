<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:255',
            'phone' => 'required|min:7|max:255',
            'email' => 'required|email|max:255',
            'referral_name' => 'required|min:3|max:255',
            'level' => 'integer'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'El nombre requerido.',
            'name.min' => 'El nombre debe tener como minimo 3 caracteres.',
            'phone.required' => 'El número de teléfono es requerido.',
            'phone.min' => 'El número de teléfono debe tener 7 digitos como minimo.',
            'email.required' => 'El correo electrónico es requerido.',
            'referral_name.required' => 'El nombre del referido es requerido.',
            'referral_name.min' => 'El nombre del referido debe tener como minimo 3 caracteres.',
            'level.required' => 'El nivel es requerido.',
            'level.integer' => 'El nivel debe ser de tipo entero.',
        ];
    }
}
