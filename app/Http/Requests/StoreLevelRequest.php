<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:255|min:3|unique:levels,level_name',
            'price' => 'required|integer',
            'description'   => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
            'name.unique' => 'El nombre ya esta en uso',
            'name.min' => 'El nombre debe tener 3 letras como minimo',
            'price.required' => 'El precio es requerido',
            'price.integer' => 'El precio debe ser numerico',
        ];
    }
}
