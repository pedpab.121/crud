<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;
use App\Http\Requests\StorePersonRequest;
use App\Models\Level;
use App\Repositories\PersonRepository;

class PersonController extends Controller
{
    /**
     * 
     */
    private $repository;

    public function __construct(PersonRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $allData = Person::paginate(5);
        return view('person.index', compact('allData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $levels = Level::latest()->get(['id', 'level_name']);
        return view('person.create', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonRequest $request)
    {
        $this->repository->create([
            'name'  => ucwords($request->name),
            'phone' => $request->phone,
            'email' => strtolower($request->email),
            'referral_name' => ucwords($request->referral_name),
            'level_id' => $request->level,
        ]);

        return redirect()
            ->route('persons.index')
            ->with('success', 'Registro exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        
        return view('person.show', compact('person'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function edit(Person $person)
    {
        $levels = Level::get(['id', 'level_name']);
        return \view('person.edit', compact('person', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
        $this->repository->update($person, [
            'name'  => ucwords($request->name),
            'phone' => $request->phone,
            'email' => strtolower($request->email),
            'referral_name' => ucwords($request->referral_name),
            'level_id'  => $request->level
        ]);

        return redirect()
            ->route('persons.index')
            ->with('success', 'El registro de ' . $person->name . ' ha sido actualizado con exito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        $person->delete();

        return redirect()
            ->route('persons.index')
            ->with('success', 'Registro de ' . $person->name . ' ha sido eliminado con exito.' );
    }
}
