<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;
use App\Http\Requests\StoreLevelRequest;
use App\Http\Requests\UpdateLevelRequest;
use App\Repositories\LevelRepository;

class LevelController extends Controller
{
    /**
     * 
     */
    private $repository;

    public function __construct(LevelRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Level::paginate(5);
        return \view('level.index', compact('allData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \view('level.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Request\StoreLevelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLevelRequest $request)
    {
        $this->repository->create([
            'level_name'    => \ucfirst($request->name),
            'price'    => $request->price,
            'description'    => \ucfirst($request->description),
        ]);

        return redirect()
            ->route('levels.index')
            ->with('success', 'Registro exitoso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function show(Level $level)
    {
        return \view('level.show', compact('level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function edit(Level $level)
    {
        return \view('level.edit', compact('level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Request\UpdateLevelRequest  $request
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLevelRequest $request, Level $level)
    {
        $this->repository->update($level, [
            'level_name' => ucfirst($request->name),
            'price' => $request->price,
            'description' => ucfirst($request->description),

        ]);
        return redirect()
            ->route('levels.index')
            ->with('success', 'Registro de ' . $level->level_name . ' ha sido actualizado con exito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Level  $level
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level $level)
    {
        $level->delete();

        return redirect()
            ->route('levels.index')
            ->with('success', 'Registro de ' . $level->level_name . ' ha sido eliminado con exito.');
    }
}
