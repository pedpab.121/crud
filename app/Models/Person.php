<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;
    /**
     * 
     */
    protected $table = 'persons';

    /**
     * 
     */
    protected $fillable = [
        'name', 'phone', 'email', 'referral_name', 'level_id'
    ];

    /**
     * Get the phone associated with the user.
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }    
}
