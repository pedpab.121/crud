<!-- resources/views/person/create.blade.php -->
@extends('layouts.app')

@section('content')
<div class="container">
  <h1 class="border-bottom font-weight-bold mt-4 mx-auto pb-2 text-black-50 text-center text-uppercase w-75">Nuevo
    Registro</h1>
  <div class="justify-content-center row">
    <div class="col-12">
    </div>
    <div class="col-12 col-md-5">
      <a class="btn btn-outline-secondary btn-sm mt-3 px-4" href="{{ route('levels.index') }}">Regresar</a>
      <div class="p-3 border mt-3 rounded">
        <form class="row" action="{{ route('levels.store') }}" method="post">
          @csrf
          <div class="mb-3 col-md-12">
            <label for="name" class="form-label">Nombre del nivel</label>
            <input type="text" class="form-control text-capitalize" id="name" name="name" placeholder="Level One.."
              value="{{ old('name') }}" />
            @error('name')
            <div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
            @enderror
          </div>
          <div class="mb-3 col-md-12">
            <label for="price" class="form-label">Precio</label>
            <input type="text" class="form-control" id="price" name="price" placeholder="20"
              value="{{ old('price') }}" />
            @error('price')
            <div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
            @enderror
          </div>
          <div class="mb-3 col-md-12">
            <label for="description" class="form-label">Descripción</label>            
            <textarea class="form-control" id="description" name="description" placeholder="Lorem ipsum dolor sit amet..."
              value="{{ old('description') }}" ></textarea>
            @error('description')
            <div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
            @enderror
          </div>
          <div class="mb-3 col-md-12 text-center mt-3">
            <button class="btn btn-outline-success btn-sm px-3 text-capitalize" type="submit">Añadir Registro</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection