<!-- resources/views/person/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="text-center font-weight-bold text-uppercase mt-3">{{ $level->level_name }}</h1>

	<div class="row justify-content-center flex-column align-items-center">
        <ul class="col-md-6 list-group mt-3">
            <li class="list-group-item">{{ $level->price }}</li>
            <li class="list-group-item">{{ $level->description ? $level->description : 'No hay descripción disponible.' }}</li>
            <li class="list-group-item text-end">
                <a class="btn btn-outline-secondary btn-sm mt-3 px-4" href="{{ route('levels.index') }}">Regresar</a>
            </li>
        </ul>
        
	</div>

</div>
@endsection