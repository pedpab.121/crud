<!-- resources/views/person/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="text-center font-weight-bold text-uppercase mt-3">{{ $person->name }}</h1>

	<div class="row justify-content-center flex-column align-items-center">
        <div class="col-md-6">
            <p class="text-center text-uppercase">Datos Personales</p>
            <ul class="list-group mt-3">
                <li class="list-group-item text-center"><strong class="d-block mb-2">Numero de teléfono</strong> {{ $person->phone }}</li>
                <li class="list-group-item text-center"><strong class="d-block mb-2">Correo electrónico</strong> {{ $person->email }}</li>
                <li class="list-group-item text-center"><strong class="d-block mb-2">Quien lo refirio</strong> {{ $person->referral_name }}</li>
                <li class="list-group-item text-center"><strong class="d-block mb-2">Nivel actual</strong> {{ $person->level->level_name }}</li>
                <li class="list-group-item text-end">
                    <a class="btn btn-outline-secondary btn-sm mt-3 px-4" href="{{ route('persons.index') }}">Regresar</a>
                </li>
            </ul>
        </div>
        
	</div>

</div>
@endsection