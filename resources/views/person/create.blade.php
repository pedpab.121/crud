<!-- resources/views/person/create.blade.php -->
@extends('layouts.app')

@section('content')
<div class="container">
  <h1 class="border-bottom font-weight-bold mt-4 mx-auto pb-2 text-black-50 text-center text-uppercase w-75">Nuevo
    Registro</h1>
  <div class="justify-content-center row">
    <div class="col-12">
    </div>
    <div class="col-12 col-md-6">
      <a class="btn btn-outline-secondary btn-sm mt-3 px-4" href="{{ route('persons.index') }}">Regresar</a>
      <div class="p-3 border mt-3 rounded">
        <form class="row" action="{{ route('persons.store') }}" method="post">
          @csrf
          <div class="mb-3 col-md-12">
            <label for="name" class="form-label">Nombre Completo</label>
            <input type="text" class="form-control text-capitalize" id="name" name="name" placeholder="Jhon Doe..." value="{{ old('name') }}" />
			@error('name')
				<div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
			@enderror
          </div>
          <div class="mb-3 col-md-6">
            <label for="phone" class="form-label">Número de Teléfono</label>
            <input type="tel" class="form-control" id="phone" name="phone" placeholder="+12555..." value="{{ old('phone') }}" />
			@error('phone')
				<div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
			@enderror			
          </div>
          <div class="mb-3 col-md-6">
            <label for="email" class="form-label">Correo Electrónico</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="example@example.com" value="{{ old('email') }}" />
			@error('email')
				<div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
			@enderror			
          </div>
          <div class="mb-3 col-md-6">
            <label for="referral_name" class="form-label">Nombre de la persona que lo refirio</label>
            <input type="text" class="form-control text-capitalize" id="referral_name" name="referral_name"
              placeholder="Jhon Doe.." value="{{ old('referral_name') }}" />
			  @error('referral_name')
			  <div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
		  @enderror			  
          </div>
          <div class="mb-3 col-md-6">
            <label for="level" class="form-label">Nivel</label>
            <select class="form-select" name="level" id="level">
              <option selected>Open this select menu</option>
              @forelse ($levels as $item)
              <option value="{{ $item->id }}">{{ $item->level_name }}</option>
              @empty
                  
              @endforelse
            </select>
			@error('level')
				<div class="alert py-1 alert-danger mt-1">{{ $message }}</div>
			@enderror
          </div>
          <div class="mb-3 col-md-12 text-center mt-3">
            <button class="btn btn-outline-success btn-sm px-3 text-capitalize" type="submit">Añadir Registro</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection