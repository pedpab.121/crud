<!-- resources/views/person/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
	<h1 class="text-center font-weight-bold text-uppercase mt-3">Crud con laravel</h1>

	<div class="row">
		<div class="col-12 text-end border-bottom pb-3">
			<a class="btn btn-outline-info btn-sm" href="{{ route('persons.create') }}">Nuevo</a>
		</div>
		<div class="col-12">
			<div class="table-responsive mt-3">
				<!-- will be used to show any messages -->
				@if ( session('success') )
					<div class="alert alert-info py-2">{{ Session::get('success') }}</div>
				@endif				
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Teléfono</th>
							<th>Correo Electrónico</th>
							<th>Nivel</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@forelse ($allData as $data)
						<tr>
							<th><a class="btn btn-sm btn-outline-dark px-3 py-0" href="{{ route('persons.show', $data) }}">{{ $loop->iteration }}</a></th>
							<td>{{ $data->name }}</td>
							<td>{{ $data->phone }}</td>
							<td>{{ $data->email }}</td>
							<td>{{ $data->level->level_name}}</td>
							<td>
								<a class="btn btn-success btn-sm" href="{{ route('persons.edit', $data) }}"><span class="sr-only">Editar</span></a>
								<form class="d-inline" action="{{ route('persons.destroy', $data) }}" method="post">
									@csrf
									@method('DELETE')
									<input class="btn btn-outline-danger btn-sm" type="submit" name="" value="Eliminar">
								</form>
							</td>
						</tr>
						@empty
						<tr>
							<th colspan="6" class="text-center">No hay datos disponibles.</th>
						</tr>
						@endforelse
					</tbody>

				</table>
				<div class="border-bottom mt-4 py-2 text-end">
					{{ $allData->links() }}
				</div>
			</div>
		</div>
	</div>

</div>
@endsection